<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>net.collabsoft.plugins.bamboo</groupId>
    <artifactId>bamboo-plugins</artifactId>
    <version>1.2.0</version>

    <organization>
        <name>CollabSoft</name>
        <url>http://www.collabsoft.net/</url>
    </organization>

    <name>Bamboo Add-ons</name>
    <packaging>pom</packaging>

    <properties>
        <licensing.enabled>true</licensing.enabled>
        <default.resources.dir>/net/collabsoft/plugins/bamboo</default.resources.dir>

        <bamboo.version>LATEST</bamboo.version>
        <bamboo.data.version>3.2.2</bamboo.data.version>
        <amps.version>5.0.3</amps.version>
        <upm.license.compatibility.version>2.3</upm.license.compatibility.version>
        <sal.api.version>2.4.0</sal.api.version>
        <ao.version>0.19.11.bamboo.4</ao.version>
        <atlassian.templaterenderer.version>1.0.5</atlassian.templaterenderer.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    </properties>

    <repositories>
        <repository>
            <id>atlassian</id>
            <name>Atlassian Repository</name>
            <url>https://maven.atlassian.com/content/groups/public/</url>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
        <repository>
            <id>repository.jboss.org-public</id>
            <name>JBoss repository</name>
            <url>https://repository.jboss.org/nexus/content/groups/public</url>
        </repository>
        <repository>
            <id>CollabSoft Maven Repository</id>
            <name>CollabSoft Maven Repository</name>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <url>https://api.bitbucket.org/1.0/repositories/collabsoft/mvn-repository/raw/releases</url>
        </repository>
        <repository>
            <id>CollabSoft Maven Repository - Snapshots</id>
            <name>CollabSoft Maven Repository</name>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <url>https://api.bitbucket.org/1.0/repositories/collabsoft/mvn-repository/raw/snapshots</url>
        </repository>
        <repository>
            <id>synergian-repo</id>
            <url>https://raw.github.com/synergian/wagon-git/releases</url>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>CollabSoft Maven Repository</id>
            <name>CollabSoft Maven Repository</name>
            <url>git:releases://https://bitbucket.org/collabsoft/mvn-repository.git</url>
        </repository>
        <snapshotRepository>
            <id>CollabSoft Maven Repository</id>
            <name>CollabSoft Maven Repository</name>
            <url>git:snapshots://https://bitbucket.org/collabsoft/mvn-repository.git</url>
        </snapshotRepository>
        <site>
            <id>Maven Site</id>
            <url>file://${basedir}/${site.basedir}/</url>
        </site>
    </distributionManagement>

    <pluginRepositories>
        <pluginRepository>
            <id>atlassian-public</id>
            <url>https://m2proxy.atlassian.com/repository/public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </pluginRepository>
        <pluginRepository>
            <id>synergian-repo</id>
            <url>https://raw.github.com/synergian/wagon-git/releases</url>
            <releases>
                <enabled>true</enabled>
            </releases>
        </pluginRepository>
    </pluginRepositories>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.bamboo</groupId>
            <artifactId>atlassian-bamboo-web</artifactId>
            <version>${bamboo.version}</version>
            <scope>provided</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-log4j12</artifactId>
                </exclusion>
            </exclusions>            
        </dependency>
        <dependency>
            <groupId>com.atlassian.util.concurrent</groupId>
            <artifactId>atlassian-util-concurrent</artifactId>
            <version>2.4.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <version>2.10.15</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
            <version>20070829</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>2.6</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.2.2-atlassian-1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>11.0.2-atlassian-01</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.5</version>
            <scope>provided</scope>
        </dependency>
                
        <!-- OSGI -->
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-bridge</artifactId>
            <version>3.2.15</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.osgi</groupId>
            <artifactId>spring-osgi-core</artifactId>
            <version>1.1.3</version>
            <scope>provided</scope>
        </dependency>

        <!-- Active Objects -->
       <dependency>
            <groupId>com.atlassian.activeobjects</groupId>
            <artifactId>activeobjects-plugin</artifactId>
            <version>${ao.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.activeobjects</groupId>
            <artifactId>activeobjects-bamboo-spi</artifactId>
            <version>${ao.version}</version>
            <scope>provided</scope>
        </dependency>

        <!-- Licensing -->
        <dependency>
            <groupId>com.atlassian.upm</groupId>
            <artifactId>plugin-license-storage-lib</artifactId>
            <version>${upm.license.compatibility.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.upm</groupId>
            <artifactId>plugin-license-storage-plugin</artifactId>
            <version>${upm.license.compatibility.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.upm</groupId>
            <artifactId>licensing-api</artifactId>
            <version>${upm.license.compatibility.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.upm</groupId>
            <artifactId>upm-api</artifactId>
            <version>${upm.license.compatibility.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.templaterenderer</groupId>
            <artifactId>atlassian-template-renderer-api</artifactId>
            <version>${atlassian.templaterenderer.version}</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-bamboo-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <productVersion>${bamboo.version}</productVersion>
                    <productDataVersion>${bamboo.data.version}</productDataVersion>
                    <pluginArtifacts>
                        <pluginArtifact>
                            <groupId>com.atlassian.activeobjects</groupId>
                            <artifactId>activeobjects-plugin</artifactId>
                            <version>${ao.version}</version>
                        </pluginArtifact>
                        <pluginArtifact>
                            <groupId>com.atlassian.activeobjects</groupId>
                            <artifactId>activeobjects-bamboo-spi</artifactId>
                            <version>${ao.version}</version>
                        </pluginArtifact>
                    </pluginArtifacts>
                    <instructions>
                        <Private-Package>com.atlassian.upm.license.storage.lib*</Private-Package>
                        <DynamicImport-Package>com.atlassian.upm.api.license.entity;version="2.0.1", com.atlassian.upm.api.license;version="2.0.1", com.atlassian.upm.api.util;version="2.0.1", com.atlassian.upm.license.storage.plugin;version="${upm.license.compatibility.version}"</DynamicImport-Package>
                    </instructions>
                    <bundledArtifacts>
                        <bundledArtifact>
                            <groupId>com.atlassian.upm</groupId>
                            <artifactId>atlassian-universal-plugin-manager-plugin</artifactId>
                            <version>1.6.3</version>
                        </bundledArtifact>
                        <bundledArtifact>
                            <groupId>com.atlassian.upm</groupId>
                            <artifactId>plugin-license-storage-plugin</artifactId>
                            <version>${upm.license.compatibility.version}</version>
                        </bundledArtifact>
                    </bundledArtifacts>
                    <testGroups>
                        <!-- There are currently no Wired Tests, ignoring -->
                        <!--testGroup>
                            <id>wired-integration</id>
                            <productIds>
                                <productId>bamboo</productId>
                            </productIds>
                            <includes>
                                <include>it/**/WiredTest*.java</include>
                            </includes>
                        </testGroup-->
                        <testGroup>
                            <id>traditional-integration</id>
                            <productIds>
                                <productId>bamboo</productId>
                            </productIds>
                            <includes>
                                <include>it/**/cuke/**/IT*Test.java</include>
                            </includes>
                        </testGroup>
                    </testGroups>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
                <configuration>
                    <source>1.6</source>
                    <target>1.6</target>
                </configuration>
            </plugin>
            
            <!--plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-storage-plugin</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.outputDirectory}</outputDirectory>
                            <includeArtifactIds>plugin-license-storage-plugin</includeArtifactIds>
                            <stripVersion>true</stripVersion>
                        </configuration>
                    </execution>
                </executions>
            </plugin-->
        </plugins>
        
        <extensions>
            <extension>
                <groupId>ar.com.synergian</groupId>
                <artifactId>wagon-git</artifactId>
                <version>0.2.3</version>
            </extension>
        </extensions>
    </build>
</project>
