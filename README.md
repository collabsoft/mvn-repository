# CollabSoft Maven Repository #

This is the official repository for CollabSoft releases.

## Usage ##

To use CollabSoft libraries in your Maven project, add the following repository information to your POM:


```
#!xml

    <repositories>
        <repository>
            <id>CollabSoft Maven Repository</id>
            <name>CollabSoft Maven Repository</name>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <url>https://api.bitbucket.org/1.0/repositories/collabsoft/mvn-repository/raw/releases</url>
        </repository>
        <repository>
            <id>CollabSoft Maven Repository - Snapshots</id>
            <name>CollabSoft Maven Repository</name>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <url>https://api.bitbucket.org/1.0/repositories/collabsoft/mvn-repository/raw/snapshots</url>
        </repository>
    </repositories>

```
